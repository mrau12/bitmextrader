#|
  This file is a part of bitmextrader project.
|#
(defsystem "bitmextrader-tests"
  :defsystem-depends-on ("prove-asdf")
  :serial t
  :depends-on ("bitmextrader" "fiveam" "prove")  
  :components ((:module "tests"
                        
                        :components (
                                     (:test-file "bitmexsignature-test")
                                     (:test-file "ws-test")
                                     (:test-file "rsi-test")
                                     (:test-file "ema-test")
                                     (:test-file "tests"))))
  :description "Test system for bitmextrader"
                                        ;:perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c))
  :perform (test-op (o c) (symbol-call :fiveam '#:run! :tests));:bitmextrader-test ; this is the .asd test file name
                                        ;:perform (test-op :after (op c) (funcall (intern #.(string :run) :prove) c))
  )
