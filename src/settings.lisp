(defpackage :settings
  (:use :cl)
  (:export :settings :settings-BITMEX_BASE_URL :settings-REST_ENDPOINT :settings-BITMEX_URL :settings-API_KEY :settings-API_SECRET :settings-VERB :settings-ENDPOINT :settings-BITMEX_SYMBOL))
(in-package :settings)

(defclass settings ()
  ((BITMEX_URL
    :initform "wss://testnet.bitmex.com" ; real url is: "wss://www.bitmex.com"
    :initarg BITMEX_URL
    :type    string
    :accessor settings-BITMEX_URL)
   (BITMEX_BASE_URL
    :initform "testnet.bitmex.com" ; real url is: "wss://www.bitmex.com"
    :initarg BITMEX_BASE_URL
    :type    string
    :accessor settings-BITMEX_BASE_URL)
   (API_KEY
    :initform "ilgNx1_H8GmBOB26jFlX9Aea"
    :initarg API_KEY
    :type    string
    :accessor settings-API_KEY)
   (API_SECRET
    :initform "u-bfQxmU9sLwjGSMRKQkMrLuaEfMeFK-0D0Eb9lJJQxM_5jB"
    :initarg API_SECRET
    :type    string
    :accessor settings-API_SECRET)   
   (REST_ENDPOINT
    :initform "/api/v1"
    :initarg REST_ENDPOINT
    :type    string
    :accessor settings-REST_ENDPOINT)

   (ENDPOINT
    :initform "/realtime"
    :initarg ENDPOINT
    :type    string
    :accessor settings-ENDPOINT)
   
   (BITMEXSYMBOL
    :initform "XBTUSD"
    :initarg BITMEXSYMBOL
    :type    string
    :accessor settings-BITMEX_SYMBOL)))
