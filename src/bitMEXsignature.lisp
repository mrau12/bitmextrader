(defpackage :bitMEXsignature
  (:use :cl :ironclad :quri)  
  (:export #:api-signature #:hmac-sha256 #:bytes->hex #:generate-path #:generate-nonce #:get-message #:api-signature2))

(in-package :bitMEXsignature)

(defun generate-path (url)
  "generates path from URL, if query component exists the appended to path else path"
  (let ((the_path (quri:uri-path (quri:uri url)))
        (the_query (quri:uri-query (quri:uri url)))) ;"http://www.ics.uci.edu/pub/ietf/uri/#Related" to "/pub/ietf/uri/"
    (if (= (length the_query) 0) the_path (concatenate 'string the_path "?" the_query))
      ;Either path is: path + '?' + query or path  e.g. "/pub/ietf/uri?guest=1"    
    ))

(defun generate-nonce ()
  "seconds elapsed from 1900 until current now, plus 36 seconds"
  (+ (get-universal-time) 36))

;; https://stackoverflow.com/questions/42445504/how-do-i-create-sha256-hmac-using-ironclad-in-common-lisp/42446211
(defun bytes->hex (bytes)
  (ironclad:byte-array-to-hex-string bytes))

(defun hmac-sha256 (secret text)
  (let ((hmac (make-hmac (ironclad:ascii-string-to-byte-array secret) :sha256)))
    (update-hmac hmac (ironclad:ascii-string-to-byte-array text))
    (hmac-digest hmac)))

;; Generates an API signature.
;; A signature is HMAC_SHA256(secret, verb + path + nonce + data), hex encoded.
;; Verb must be uppercased, url is relative, nonce must be an increasing 64-bit integer
;; and the data, if present, must be JSON without whitespace between keys.

;;   Example:
;; verb=POST
;; url=/api/v1/order
;; nonce=1416993995705
;; data={"symbol":"XBTZ14","quantity":1,"price":395.01}
;; signature = HEX(HMAC_SHA256(secret, 'POST/api/v1/order1416993995705{"symbol":"XBTZ14","quantity":1,"price":395.01}'))
;; An example: key = (api-signature "secret" "GET" "/realtime" 100 "hey") --> '6efb114ad309afd85175a384f04dec5180ee763e5d682a6b62e5a083a34815a5'

(defun api-signature (secret verb url nonce data)
    (let ((message (concatenate 'string verb (generate-path url) (write-to-string nonce) data)))    
      (bytes->hex (hmac-sha256 secret message))))

;; Alternative functions for rest
(defun get-message (verb url data nonce)
  (concatenate 'string verb url data (write-to-string nonce)))

(defun api-signature2 (secret message)
  (bytes->hex (hmac-sha256 secret message)))
