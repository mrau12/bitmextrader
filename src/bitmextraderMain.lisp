(defpackage bitmextraderMain
  (:use :cl :rest :rsi :ema  :settings :cl-date-time-parser :cl-async :blackbird))
(in-package :bitmextraderMain)

;; Setting defaults
(defun get-default-settings ()
  (make-instance 'settings))

;; Filter out information from candles plist
;; candles: plist filled with info like close, open, etc
;; filter: list of keys we want to output i.e. '("close")
;; return: list of values (list 12 23 435 45)
(defun getFilteredList (candles filter)
  (loop for x in candles append (pfilter-by-keys filter x)))

;; List -> PList -> PList
;; Returns a new plist with only the keys correspondent to the given
;; keys.
(defun pfilter-by-keys (keys plist)
  (loop for (k v) on plist by #'cddr
     when (member k keys :test #'equal)
     collect v ))

;; filter: list of keys we want to output i.e. '("close")
;; return: list of values with timestamps ("2019-09-26T04:55:00.000Z" 8456.5 "2019-09-26T04:50:00.000Z" 8458.5
;; "2019-09-26T04:45:00.000Z" 8468 "2019-09-26T04:40:00.000Z" 8468)
(defun getLastClosesWithStamp (candles)
  (getFilteredList candles '("timestamp" "close")))


;; Input: ("2019-09-27T01:03:00.000Z" 8171.5 "2019-09-27T01:02:00.000Z" 8171.5)  
;; Output: (8171.5 8171.5)
(defun collect-prices-from-pricestamps (xs)
  (loop for x in xs
     for i from 1
     if (evenp i)
     collect x))


;; Returns n prev closes prices before current close price
;; Input:
;; n: How many prev close prices to return
;; time_interval: 1m, 5m, 1h

(defun getcurrent+prevprices (n time_interval)
  (let* ((current (getLastClosesWithStamp (getCandles "1" time_interval (make-instance 'settings)) ))
         (previous (cddr (getLastClosesWithStamp (getCandlesBeforeStamp (write-to-string (+ n 1))  time_interval (first current) (make-instance 'settings))))))
                                        ;(list current previous) ; check if prices are correct and have correct timestamps
    (flatten (list (collect-prices-from-pricestamps current) (collect-prices-from-pricestamps previous)))))

;; if list empty '() returns 0
(defun get-min (lst) ; takes a list return the minimum value
  (loop for x in lst
     minimize x) )

;; firstTwo a timestamp with its close price, the most current bitcoin close price with stamp of in-putted list

;; Input: previous_close_priceS_with_stampS:
;; (list "2019-09-26T04:05:00.000Z" 8477 "2019-09-26T04:00:00.000Z" 8477
;; "2019-09-26T03:55:00.000Z" 8478.5 "2019-09-26T03:50:00.000Z" 8485
;; "2019-09-26T03:45:00.000Z" 8485 "2019-09-26T03:40:00.000Z" 8489.5)
;; output:
;; ("2019-09-26T04:05:00.000Z" 8477)

(defun firstTwo (previous_close_priceS_with_stampS)
  (let ((prices_with_stamps previous_close_priceS_with_stampS))
    (flatten (list (first prices_with_stamps) (second prices_with_stamps)))))


;; Remove nested parentheses
(defun flatten (l)
  (cond ((null l) nil)
        ((atom l) (list l))
        (t (loop for a in l appending (flatten a)))))

;; Get last 20 closes
(defun getLastCloses (candles)
  (rsi:getFilteredList candles '("close")))

;; enter trade and buy at
(defun enterTrade (percent default_settings)
  (createOrder default_settings "Limit" "Buy" "USD" (%Account percent default_settings)))

(defun %Account (percent default_settings)
  (* (getWallet default_settings) (/ percent 100)))

;; Buy at 9550, current ema is 9600, ratio is 1/1
;; return 9500 (we sell at either 9600 or 9500)

;; return stop_price at loss, ema is high price to exit while stop evaluates to low price to stop
(defun stop (ratio current_ema price_bought_at)
  (let* ((target (- current_ema price_bought_at))
         (low_price (* (/ 1 ratio) (- price_bought_at target))))
    low_price))

(defun currentRsiIsLowest (last41prices)
  (if (< (first last41prices) (get-min (rsi:calc20Rsi (rest last41prices)))) t nil))


;; Current price is lowest of the previous 20 close prices
;; prices: number of previous prices 

;; Output: T when current is lowest of the last n previous prices

;; ex: (8143 8160.5 8159 8158.5 8144 8144 8142.5 8143.5 8144.5 8146 8141 8140.5 8121
;; 8124.5 8157.5 8157.5 8157.5 8158 8159.5 8151.5 8164) 8143 aka the current price is not the lowest therefore nil

(defun isCurrentPriceIsLowest (prices)
  (let  ((the_current_price (first prices)))
    (if (< the_current_price (get-min (rest prices))) t nil)))

(defun run (n sec )
  (loop
     with last41prices = (GETCURRENT+PREVPRICES 41 "1m")
     with last20prices = (subseq last41prices 0 20)
     with trigger  = (and (isCurrentPriceIsLowest last20prices) (not (currentRsiIsLowest last41prices)))
     repeat n
     while (not trigger)
     do (as:start-event-loop
         (lambda ()
           (as:delay
            (lambda ()
              (progn (setf last41prices (GETCURRENT+PREVPRICES 41 "1m"))
                     (setf last20prices (subseq last41prices 0 20))
                     (setf trigger (and (isCurrentPriceIsLowest last20prices) (not (currentRsiIsLowest last41prices))))
                     (with-open-file (str "/home/mike/Desktop/firstPartAlgorithm.txt"
                                          :direction :output
                                          :if-exists :append
                                          :if-does-not-exist :create)
                       (format str " The current universal time: ~a~%" (get-universal-time))
                       (format str " The last 40 prices ~a~%" last41prices)
                       (format str " The last 20 prices ~a~%" last20prices)
                       (format str " The current price is lowest? ~a~%" (isCurrentPriceIsLowest last20prices))
                       (format str " The current rsi is not lowest? ~a~%" (not (currentRsiIsLowest last41prices)))
                       (format str " The current trigger? ~a~%~%" (and (isCurrentPriceIsLowest last20prices) (not (currentRsiIsLowest last41prices)))))
                     )) :time sec)))))

;; (defun trade (percent prices)
;;   (let ((current_ema (seed-ema prices  (/ 2 (+ 20 1)) (sma prices) ))
;;         (price_bought_at ))
;;       (progn         
;;         (createStopMarketOrder default_settings "100"  (stop 1 current_ema price_bought_at))
;;         (createLimitOrder default_settings "100" price))))

