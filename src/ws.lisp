(defpackage :ws
  (:use :cl :cl-json :settings :bitMEXsignature :websocket-driver-client :trivia :jsown)
  (:export #:publicQuery #:privateQuery))   
(in-package :ws)

(defvar *message-json-out* (make-string-output-stream))

(defun initial-auth-request (API_KEY expires signature)   
  (cl-json:encode-json-plist-to-string (list "op" "authKeyExpires" "args" (list API_KEY expires signature))))

(defun get-default-settings ()
  (make-instance 'settings))

(defun make-client-with-testBitmex (some_settings)
  (wsd:make-client (uiop:strcat (settings-BITMEX_URL some_settings) (settings-ENDPOINT some_settings))))

(defun get-signature (some_settings verb expires data)
  (bitMEXsignature:api-signature (settings-API_SECRET some_settings) verb (settings-ENDPOINT some_settings) expires data))

(defun get-auth-request (some_settings expires signature)
  (initial-auth-request  (settings-API_KEY some_settings) expires signature))

(defun websocket-auth (the_client request_auth subscriptionTopic)
  (progn (wsd:start-connection the_client)
         (wsd:send the_client request_auth)
         (wsd:send the_client (cl-json:encode-json-plist-to-string (list "op" "subscribe" "args" subscriptionTopic)))
        (wsd:on :message the_client
                (lambda (message)
                  (json:decode-json-from-string message)))
        (sleep 10)
        (wsd:close-connection the_client)))

;; (privateQuery "GET" "" "/realtime" (list "position"))
;; HOW DO I SOMETHING LIKE:  (privateQuery "GET" "" "/realtime" (list "orders" "symbol:XBTZ14" "quantity:1" "price:395.01"))
; Got: {"success":true,"request":{"op":"authKeyExpires","args":["ilgNx1_H8GmBOB26jFlX9Aea",3771644156,"037bea90c79a2c162ac1aa81fec640bd4920749511efedfe951bae99f6621996"]}}
; Got: {"status":400,"error":"Unknown table: orders","meta":{},"request":{"op":"subscribe","args":["orders","symbol:XBTZ14","quantity:1","price:395.01"]}}
(defun  privateQuery(verb data function subscriptionTopic) ;auth-with-data
  (let* ((default_settings (get-default-settings))
         (expires (bitMEXsignature:generate-nonce))
         (signature (get-signature default_settings verb expires data)))
    (websocket-auth (wsd:make-client (uiop:strcat (settings-BITMEX_URL default_settings) function))
                    (get-auth-request default_settings expires signature)
                    subscriptionTopic)))

;; SubscriptionTopic: (list "orderBookL2_25:XBTUSD")
;; Data: {"symbol":"XBTZ14","quantity":1,"price":395.01}
;; Example: (pulicQuery "/realtime" (list "tradeBin1m"))
(defun publicQuery (function subscriptionTopic)
  (let* ((json_output_stream (make-string-output-stream))
         (default_settings (get-default-settings))
         (client (wsd:make-client (uiop:strcat (settings-BITMEX_URL default_settings) function))))
    (wsd:start-connection client)           
    (wsd:send client (cl-json:encode-json-plist-to-string (list "op" "subscribe" "args" subscriptionTopic)))           
    
    (wsd:on :message client (lambda (message) (format json_output_stream message)))            
    (sleep 15)
    (wsd:close-connection client)
    (json-streams:json-parse-multiple (get-output-stream-string json_output_stream))))

(defun publicQueryTest ()
  ( publicQuery "/realtime" (list "instrument:XBTUSD")))

(defun publicQueryTest2 (function subscriptionTopic)
  (let* ((json_output_stream (make-string-output-stream))
         (default_settings (get-default-settings))
         (client (wsd:make-client (uiop:strcat (settings-BITMEX_URL default_settings) function))))
    (wsd:start-connection client)           
    (wsd:send client (cl-json:encode-json-plist-to-string (list "op" "subscribe" "args" subscriptionTopic)))           
    
    (wsd:on :message client (lambda (message) (format json_output_stream message)))            
    
    (wsd:close-connection client)
    (jsown:parse (get-output-stream-string json_output_stream))
    ))

