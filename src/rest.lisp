(defpackage :rest
  (:use :cl :drakma :quri :yason :trivia :bitMEXsignature :settings)
  (:export #:getCandlesFiltered #:getCandlesBeforeStamp  #:getCandles #:createOrder #:getWallet #:createStopMarketOrder #:createLimitOrder))   
(in-package :rest)

 ;;; The headers exchanged between Drakma and the HTTP server
(setf drakma:*header-stream* *standard-output*)

;; If internet is drops, then continue
(defun the-http-request (uri)
  (handler-bind ((usocket:TIMEOUT-ERROR #'continue))
    ;Needed to add url-decode to properly decode a Percent-Encoded string i.e. "%2Ffoo%E3%81%82" -> "/fooあ" 
    (drakma:http-request (quri:url-decode (render-uri uri)) :want-stream t)))

(defun requestPub (uri)
  (let* ((stream (the-http-request uri)))
                                        
    
    (setf (flexi-streams:flexi-stream-external-format stream) :utf-8)   
                                        ;(the-http-request stream)
    (yason:parse stream :object-as :plist)) );:hash-table ;yason:parse stream :object-as :plist



(defun makeCandlesParameters (default_settings &optional (count "100") (timeInterval "5m"))
  (list (cons "binSize" timeInterval) (cons "symbol" (settings-BITMEX_SYMBOL default_settings)) (cons "count" count) (cons "reverse" "true")))

(defun getCandles (count timeInterval default_settings)
  (requestPub (merge-uris (make-uri                     
                        :path "/api/v1/trade/bucketed"
                        :query (makeCandlesParameters default_settings count timeInterval))
                          (make-uri :scheme "https" :host (settings-BITMEX_BASE_URL default_settings)))))

;; Arguments:
;; count: string number, i.e "100"
;; timeinterval: time between candles, i.e. "1m", "5m", etcetera.
;; endtime: the time from which to get the first candle.
(defun getCandlesBeforeStamp (count timeInterval endStamp default_settings)
  (requestPub (merge-uris (make-uri                     
                           :path "/api/v1/trade/bucketed"
                           :query (list
                                   (cons "binSize" timeInterval)
                                   (cons "symbol" (settings-BITMEX_SYMBOL default_settings))
                                   (cons "count" count)
                                   (cons "reverse" "true")
                                   (cons "endTime" (format nil endStamp))))
                          (make-uri :scheme "https" :host (settings-BITMEX_BASE_URL default_settings)))))


(defun getTicker (default_settings)  
  (requestPub (merge-uris (make-uri                     
                        :path "/api/v1/instrument"
                        :query (list (cons "symbol" (settings-BITMEX_SYMBOL default_settings))))
                          (make-uri :scheme "https" :host (settings-BITMEX_BASE_URL default_settings))
                          )))

(defun filter (ht list_keys)
  (loop for k being each hash-key of ht
    using (hash-value v)
     when (not (not (find k list_keys :test #'string=))) ;; not not - coerce into boolean
     collect (cons k v)))


;;;;;;;;;; PUBLIC QUERIES ;;;;;;;;;;;;


(defun getTickerFiltered ()
  (mapcar
   #'(lambda (ht)
       (filter ht (list "symbol" "lastPrice" "bidPrice" "askPrice" "highPrice" "lowPrice")))
   (getTicker (make-instance 'settings))))

(defun getCandleWithFilter (default_settings count timeInterval)
  (mapcar
   #'(lambda (ht) (filter ht (list  "open" "high" "close" "low" )))
   (getCandles count timeInterval default_settings)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun alist->jsonString (pl)
  (with-output-to-string (*standard-output*) (yason:encode-alist pl)))

;; verb=POST
;; endpoint=order
;; i.e (/api/v1 already retreieved from settings => /api/v1/order)
;; nonce=1416993995705
;; data={"symbol":"XBTZ14","quantity":1}

;;; extraHeaders (alist): '(("api-signature" . $sign) ("api-key" . $apiKey) ("api-nonce" . $nonce))

(defun requestPrivate (uri extraHeaders verb_symb)  
  (let ((stream (handler-bind ((usocket:TIMEOUT-ERROR #'continue))
                  (drakma:http-request (quri:url-decode (render-uri uri)) :method verb_symb :want-stream t :additional-headers extraHeaders))))
    
    (setf (flexi-streams:flexi-stream-external-format stream) :utf-8)   
    (yason:parse stream :object-as :hash-table)))

 ;;; Verb (aka method) = "GET", "POST", etc  Endpoint = "/order"
 ;;; default settings = (make-instance 'settings)
(defun authQuery (uri verb default_settings &optional (data ""))
  (let* ((nonce (bitMEXsignature:generate-nonce))
         (url_sign (uri-path uri))
         (msg (bitMEXsignature:get-message verb url_sign data nonce))
         (signature (api-signature2 (settings-API_SECRET default_settings) msg))
         (headers (list
                   (cons "api-signature" signature)
                   (cons "api-key" (settings:settings-API_KEY default_settings) )
                   (cons "api-nonce" nonce)
                   (cons "Connection" "Keep-Alive")
                   (cons "Keep-Alive" "90"))))
    (print (concatenate 'string "signature: " signature))
    (print (concatenate 'string "message: " msg))
    (print (concatenate 'string "url/uri: " (render-uri uri)))
    
    (trivia:match verb
      ("GET" (requestPrivate uri headers ':get))
      ("HEAD"  (requestPrivate uri headers ':head))
      ("OPTIONS" (requestPrivate uri headers ':options))
      ("PUT"  (requestPrivate uri headers ':put))
      ("POST" (requestPrivate uri headers ':post))
      ("DELETE"  (requestPrivate uri headers ':delete))      
      (t (print "error: verb does not exist")))))


;;;;;;;;;; PRIVATE QUERIES ;;;;;;;;;;;;

;;; Get Orders
;;; Get last 100 orders
;;; Return: orders hash table (from the past to the present)

;;; Todo filter on:if  order['orderID'] == orderID Return: order
(defun getOrder ( &optional (count "100") default_settings) ;orderID
  (let ((the_uri (make-uri :scheme "https"
                           :query (list
                                   (cons "symbol" (settings-BITMEX_SYMBOL default_settings))
                                   (cons "count" count)
                                   (cons "reverse" "true"))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/order")))

    (authQuery the_uri "GET" default_settings (concatenate 'string "?" (uri-query the_uri)) )))


;;; Get Open Orders
;;; Get open orders from the last 100 orders
;;; Return: Open Order Hash tables

;;; Todo: filter on
;;; if: order['ordStatus'] == 'New' || order['ordStatus'] == 'PartiallyFilled'
;;; Return: openOrders[] = order

(defun getOpenOrders (default_settings)
  (let ((the_uri (make-uri :scheme "https"
                           :query (list
                                   (cons "symbol" (settings-BITMEX_SYMBOL default_settings))                                   
                                   (cons "reverse" "true"))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/order")))

    (authQuery the_uri "GET" default_settings (concatenate 'string "?" (uri-query the_uri)))))

;;; Get Open Positions
;;; Get all your open positions
;;; Return open positions hash tables


;;; TODO:
;; foreach(positions as position) 
;; if: (position['isOpen'] && position['isOpen'] == true) then: openPositions[] = position

(defun getOpenPositions (default_settings)
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "symbol" (settings-BITMEX_SYMBOL default_settings)))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/position")))

    (authQuery the_uri "GET" default_settings (concatenate 'string "?" (uri-query the_uri)))))

;;; Get Open Positions
;;; Get all your open positions
;;; Return open positions hash tables

(defun getPositions (default_settings)
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "symbol" (settings-BITMEX_SYMBOL default_settings)))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/position")))

    (authQuery the_uri "GET" default_settings (concatenate 'string "?" (uri-query the_uri)))))

;;; Close Position
;;; Close open position
;;; HashTable

(defun closePosition (default_settings price)
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "symbol" (settings-BITMEX_SYMBOL default_settings))
                                        (cons "price" price))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/order/closePosition")))

    (authQuery the_uri "POST" default_settings (concatenate 'string "?" (uri-query the_uri)))))

;;; Edit Order Price
;;; Edit you open order price
;;; param1: orderID    Order ID
;;; param2: price      new price
;;; Return new order ht
(defun editOrderPrice (default_settings orderID price)
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "orderID" orderID) (cons "price" price))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/order")))

    (authQuery the_uri "PUT" default_settings (concatenate 'string "?" (uri-query the_uri)))))

 ;; Create Order

 ;; Create new order

 ;; param type can be "Limit"
 ;; param side can be "Buy" or "Sell"
 ;; param price BTC price in USD
 ;; param quantity should be in USD (number of contracts)
 ;; param maker forces platform to complete your order as a 'maker' only

;; return new order HT

;;; TODO: if maker true then data['params']['execInst'] = "ParticipateDoNotInitiate"
(defun createOrder (default_settings type side price quantity &optional (maker "false"))
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "symbol" (settings-BITMEX_SYMBOL default_settings))
                                        (cons "ordType" type)
                                        (cons "side" side)
                                        (cons "price" price)
                                        (cons "orderQty" quantity))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/order")))

    (authQuery the_uri "POST" default_settings (concatenate 'string "?" (uri-query the_uri)))))



 ;; Create Limit Order

 ;; Create new limit order

 ;; param price number of contracts
 ;; param quantity (number of contracts) positive for buy and negative for sell
 ;; param $maker forces platform to complete your order as a 'maker' only

 ;; return new order HT


;;; TODO:if instructions are true then  data['params']['execInst'] = instructions
(defun createLimitOrder (default_settings quantity price &optional (instructions "false"))
  
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "symbol" (settings-BITMEX_SYMBOL default_settings))                                       
                                        (cons "price" price)
                                        (cons "orderQty" quantity)
                                        (cons "ordType" "Limit"))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/order")))

    (authQuery the_uri "POST" default_settings (concatenate 'string "?" (uri-query the_uri)))))

;;; Create Stop Market Order
;;; param stopPrice BTC trigger price
;;; param quantity should be in USD (number of contracts)

;;; TODO:
(defun createStopMarketOrder (default_settings quantity stopPrice &optional (instructions "false"))
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "symbol" (settings-BITMEX_SYMBOL default_settings))                                       
                                        (cons "stopPx" stopPrice)
                                        (cons "orderQty" quantity)
                                        (cons "ordType" "Stop"))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/order")))

    (authQuery the_uri "POST" default_settings (concatenate 'string "?" (uri-query the_uri)))))


;;; Create Stop Limit Order

(defun createStopLimitOrder (default_settings quantity stopPrice price &optional (instructions "false"))
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "symbol" (settings-BITMEX_SYMBOL default_settings))                                       
                                        (cons "stopPx" stopPrice)
                                        (cons "price" price)
                                        (cons "orderQty" quantity)
                                        (cons "ordType" "StopLimit"))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/order")))

    (authQuery the_uri "POST" default_settings (concatenate 'string "?" (uri-query the_uri)))))

;;; Cancel All Open Orders

(defun cancelAllOpenOrders (default_settings &optional (text ""))
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "symbol" (settings-BITMEX_SYMBOL default_settings))                                       
                                         (cons "text" text))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/order/all")))

    (authQuery the_uri "DELETE" default_settings (concatenate 'string "?" (uri-query the_uri)))))

;;; Get your account wallet
(defun getWallet (default_settings)
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "currency" "XBt"))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/user/wallet")))

    (authQuery the_uri "GET" default_settings (concatenate 'string "?" (uri-query the_uri)))))

;;; Get your account margin
(defun getMargin (default_settings)
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "currency" "XBt"))
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/user/margin")))

    (authQuery the_uri "GET" default_settings (concatenate 'string "?" (uri-query the_uri)))))


;;; Get L2 Order Book
(defun getOrderBook (default_settings &optional (depth "25"))
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "depth" depth)
                                        (cons "symbol" (settings-BITMEX_SYMBOL default_settings)) )
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/orderBook/L2")))

    (authQuery the_uri "GET" default_settings (concatenate 'string "?" (uri-query the_uri)))))

(defun setLeverage (default_settings leverage)
  (let ((the_uri (make-uri :scheme "https"
                           :query (list (cons "leverage" leverage)
                                        (cons "symbol" (settings-BITMEX_SYMBOL default_settings)) )
                           :host (settings-BITMEX_BASE_URL default_settings)
                           :path "/api/v1/position/leverage")))

    (authQuery the_uri "POST" default_settings (concatenate 'string "?" (uri-query the_uri)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
