(defpackage :rsi
  (:use :cl :ws :settings :rest)
  (:export  #:getFilteredList #:calc20Rsi))
(in-package :rsi)

(defun getRsi (candles)
  (calcRsi (getFilteredList candles '("close"))))

;; Gets the rsi, note it is not smooth rsi so value will be different
;; test with, values used in rsi video:
;; (list 46.1250 47.1250 46.4375 46.9375 44.9375 44.2500 44.6250 45.7500 47.8125 47.5625 47.0000 44.5625 46.3125 47.6875 46.6875) => 51.779
;; http://cns.bu.edu/~gsc/CN710/fincast/Technical%20_indicators/Relative%20Strength%20Index%20(RSI).htm
(defun calcRsi (closeList)
  (let ((gainAmount 0)
        (lossAmount 0))
    ; (print closeList)
    (mapcar  #'(lambda (x y)
                 (if (> x y)
                     (incf gainAmount (- x y))
                     (incf lossAmount (- y x))))
             (cdr closeList) (allExceptLast closeList))
    (let ((avgGain (/ gainAmount (list-length closeList)))
          (avgLoss (/ lossAmount (list-length closeList))))
      (if (= avgLoss 0)
          100 ;rsi
          (- 100 (/ 100 (+ 1 (/ avgGain avgLoss))))
          ;rsi
          ))))


;; Filter out information from candles plist
;; candles: plist filled with info like close, open, etc
;; filter: list of keys we want to output i.e. '("close")
;; return: list of values (list 12 23 435 45)
(defun getFilteredList (candles filter)
  (loop for x in candles append (pfilter-by-keys filter x)))

;; List -> PList -> PList
;; Returns a new plist with only the keys correspondent to the given
;; keys.
(defun pfilter-by-keys (keys plist)
  (loop for (k v) on plist by #'cddr
     when (member k keys :test #'equal)
     collect v ))

(defun allExceptLast (list)
  (loop for l on list
     while (rest l)
     collect (first l)))

;; Input: '(1 2 3 4 5 ... 40)
;; Output: list of lists of prices '('(1 2 3 ... 20) '(2 3 4 ... 21) '(3 4 5 ... 22) ... n) where n is 20 lists
(defun 40pricesToRsiLists (last40prices)
  (loop for i from 0 to 20
     collect (subseq last40prices i (+ i 20))))

;; Input: '(1 2 3 4 5 ... 40) last 40 prices
;; Output: '(100 101 202 101 ... n) n = 21
(defun calc20Rsi (last40prices)
  (mapcar #'(lambda (x) (calcRsi x))
          (40pricesToRsiLists last40prices)))


