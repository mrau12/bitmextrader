(defpackage :ema
  (:use :cl :ws :settings :rest :rsi )
  (:export #:seed-ema #:next-ema  #:sma))
(in-package :ema)

(defun ema-formula (price k prev_ema)
  (+ (* price k)  (* prev_ema (- 1 k) )))

(defun sma (filteredCloseList)
  (/ (reduce #'+ filteredCloseList) (length filteredCloseList)))

;;; Exponential Moving Average: A moving average places a greater (and exponential) weight and significance on the most recent data points.

;; Input:
;;; prices: price over a period of time
;;; k: (/ 2 (+ 20 1)  The weighting constant. i.e. Ex1: A 10-period EMA can also be called an 18.18% EMA.
;;; Ex2: A 20-period EMA applies a 9.52% weighting to the most recent price (2/(20+1) = .0952)
;;; first_sma: The value to seed ema, so 10-day ema would use 10-day sma for its seed value.

;; Output: Vector #(8000 8001)
(defun seed-ema (prices k first_sma)
  (let ((prev_ema first_sma))
    (map 'list
         #'(lambda (price)
             (progn
               (setf prev_ema (ema-formula price k prev_ema))
                 prev_ema))
         prices)))

;; (getLastCloses (getCandles "20" "5m" (make-instance 'settings)))
;; closes: all close prices needed to calc the last k ema's

;; Evaluates to the next ema, input immediate last ema, k constant smooth factor and current close 
(defun next-ema (curr_close_price k prev_ema)
  (+ (* k (- curr_close_price prev_ema)) prev_ema))

