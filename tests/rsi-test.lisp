(defpackage rsi-test (:use :cl :prove))
(in-package :rsi-test)

(prove:plan 2)

;; Check calcRSI is accurate
(prove:is (calcRsi (list 46.1250 47.1250 46.4375 46.9375 44.9375 44.2500 44.6250 45.7500 47.8125 47.5625 47.0000 44.5625 46.3125 47.6875 46.6875)) 51.778656)

;; Test some input candles
(prove:is (getrsi '(("timestamp" "2019-08-11T08:05:00.000Z" "symbol" "XBTUSD" "open" 11352 "high"
                     11352 "low" 11338.5 "close" 11345 "trades" 246 "volume" 136537 "vwap"
                     11341.727 "lastSize" 52 "turnover" 1203969930 "homeNotional" 12.0397
                     "foreignNotional" 136537)
                    ("timestamp" "2019-08-11T08:00:00.000Z" "symbol" "XBTUSD" "open" 11341 "high"
                     11356.5 "low" 11341.5 "close" 11352 "trades" 135 "volume" 140316 "vwap"
                     11355.894 "lastSize" 50 "turnover" 1235753311 "homeNotional" 12.357533
                     "foreignNotional" 140316)
                    ("timestamp" "2019-08-11T07:55:00.000Z" "symbol" "XBTUSD" "open" 11349.5
                     "high" 11353 "low" 11340.5 "close" 11341 "trades" 261 "volume" 170517 "vwap"
                     11350.738 "lastSize" 41 "turnover" 1502416544 "homeNotional" 15.024165
                     "foreignNotional" 170517)
                    ("timestamp" "2019-08-11T07:50:00.000Z" "symbol" "XBTUSD" "open" 11340.5
                     "high" 11380 "low" 11340.5 "close" 11349.5 "trades" 324 "volume" 767806
                     "vwap" 11363.637 "lastSize" 10 "turnover" 6757113287 "homeNotional" 67.57114
                     "foreignNotional" 767806)
                    ("timestamp" "2019-08-11T07:45:00.000Z" "symbol" "XBTUSD" "open" 11341 "high"
                     11341 "low" 11340.5 "close" 11340.5 "trades" 13 "volume" 868 "vwap" 11340.5
                     "lastSize" 118 "turnover" 7654024 "homeNotional" 0.07654024 "foreignNotional"
                     868)
                    ("timestamp" "2019-08-11T07:40:00.000Z" "symbol" "XBTUSD" "open" 11342.5
                     "high" 11341 "low" 11340 "close" 11341 "trades" 35 "volume" 2212 "vwap"
                     11340.5 "lastSize" 696 "turnover" 19505416 "homeNotional" 0.19505416
                     "foreignNotional" 2212)
                    ("timestamp" "2019-08-11T07:35:00.000Z" "symbol" "XBTUSD" "open" 11341.5
                     "high" 11342.5 "low" 11340.5 "close" 11342.5 "trades" 108 "volume" 38697
                     "vwap" 11343.013 "lastSize" 3531 "turnover" 341181502 "homeNotional" 3.411815
                     "foreignNotional" 38697)
                    ("timestamp" "2019-08-11T07:30:00.000Z" "symbol" "XBTUSD" "open" 11340 "high"
                     11342.5 "low" 11328.5 "close" 11341.5 "trades" 361 "volume" 152476 "vwap"
                     11341.727 "lastSize" 200 "turnover" 1344456165 "homeNotional" 13.444562
                     "foreignNotional" 152476)
                    ("timestamp" "2019-08-11T07:25:00.000Z" "symbol" "XBTUSD" "open" 11339 "high"
                     11340 "low" 11337.5 "close" 11340 "trades" 58 "volume" 24879 "vwap" 11340.44
                     "lastSize" 1 "turnover" 219402995 "homeNotional" 2.19403 "foreignNotional"
                     24879)
                    ("timestamp" "2019-08-11T07:20:00.000Z" "symbol" "XBTUSD" "open" 11330 "high"
                     11340 "low" 11326.5 "close" 11339 "trades" 273 "volume" 55919 "vwap"
                     11336.583 "lastSize" 9 "turnover" 493289470 "homeNotional" 4.9328947
                     "foreignNotional" 55919)
                    ("timestamp" "2019-08-11T07:15:00.000Z" "symbol" "XBTUSD" "open" 11329 "high"
                     11334 "low" 11329 "close" 11330 "trades" 151 "volume" 54066 "vwap" 11331.444
                     "lastSize" 50 "turnover" 477134368 "homeNotional" 4.7713437 "foreignNotional"
                     54066)
                    ("timestamp" "2019-08-11T07:10:00.000Z" "symbol" "XBTUSD" "open" 11329 "high"
                     11334 "low" 11329 "close" 11329 "trades" 54 "volume" 4516 "vwap" 11330.161
                     "lastSize" 5 "turnover" 39858390 "homeNotional" 0.3985839 "foreignNotional"
                     4516)
                    ("timestamp" "2019-08-11T07:05:00.000Z" "symbol" "XBTUSD" "open" 11327 "high"
                     11334 "low" 11326.5 "close" 11329 "trades" 150 "volume" 45857 "vwap"
                     11332.729 "lastSize" 100 "turnover" 404669933 "homeNotional" 4.0466995
                     "foreignNotional" 45857)
                    ("timestamp" "2019-08-11T07:00:00.000Z" "symbol" "XBTUSD" "open" 11326.5
                     "high" 11334 "low" 11326 "close" 11327 "trades" 136 "volume" 25690 "vwap"
                     11327.594 "lastSize" 2 "turnover" 226797102 "homeNotional" 2.267971
                     "foreignNotional" 25690))) 33.018867 )

(finalize)
