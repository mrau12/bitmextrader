(defpackage bitmexsignature-test
  (:use :cl  :prove))
(in-package :bitmexsignature-test)

(prove:plan 6)
(prove:is (generate-path "https://www.example.com/blog/category/individual-article-name/") "/blog/category/individual-article-name/")
(prove:is (generate-path "https://www.example.com/solutions?user=123&color=blue") "/solutions?user=123&color=blue")

;; hmac-sha256 example created from https://www.freeformatter.com/hmac-generator.html
(prove:is (bytes->hex (bitMEXsignature:hmac-sha256 "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" "dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd")) "08ecf142c747fe3d4c4434eae72ea97b566991baf09dfbf5517038a3813fffd4")

;; API signature
(prove:is (api-signature "chNOOS4KvNXR_Xq4k4c9qsfoKWvnDecLATCRlcBwyKDYnWgO" "GET" "/api/v1/instrument?filter=%7B%22symbol%22%3A+%22XBTM15%22%7D" 1518064237  "") "e2f422547eecb5b3cb29ade2127e21b858b235b386bfa45e1c1756eb3383919f")
(prove:is (api-signature "chNOOS4KvNXR_Xq4k4c9qsfoKWvnDecLATCRlcBwyKDYnWgO" "GET" "/api/v1/instrument?filter=%7B%22symbol%22%3A+%22XBTM15%22%7D" 1518064237  "") "e2f422547eecb5b3cb29ade2127e21b858b235b386bfa45e1c1756eb3383919f")
(prove:is (api-signature "chNOOS4KvNXR_Xq4k4c9qsfoKWvnDecLATCRlcBwyKDYnWgO" "POST" "/api/v1/order" 1518064238 "{\"symbol\":\"XBTM15\",\"price\":219.0,\"clOrdID\":\"mm_bitmex_1a/oemUeQ4CAJZgP3fjHsA\",\"orderQty\":98}") "1749cd2ccae4aa49048ae09f0b95110cee706e0944e6a14ad0b3a8cb45bd336b")


(finalize)
