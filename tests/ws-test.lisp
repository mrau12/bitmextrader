(defpackage ws-test
  (:use :cl :ws :bitMEXsignature)  
 
  )
(in-package :ws-test)


(defun test1 ()
  (let* ((client (wsd:make-client (uiop:strcat "wss://testnet.bitmex.com" "/realtime")))
        (expires (bitMEXsignature:generate-nonce))
        (signature (bitMEXsignature:api-signature
                    "u-bfQxmU9sLwjGSMRKQkMrLuaEfMeFK-0D0Eb9lJJQxM_5jB"
                    "GET"
                    "/realtime"
                    expires
                    "")))
   (progn (wsd:start-connection client)
          (wsd:send client (cl-json:encode-json-plist-to-string (list "op" "authKeyExpires" "args" (list "ilgNx1_H8GmBOB26jFlX9Aea" expires signature))))
          (wsd:on :message client
                  (lambda (message)
                    (format t "~&Got: ~A~%" message)))
          (wsd:send client (cl-json:encode-json-plist-to-string (list "op" "subscribe" "args" "position")))
          (wsd:on :message client
                  (lambda (message)
                    (format t "~&Got: ~A~%" message)))
          (wsd:close-connection client))))
;; Subscribe to realtime
(defun test2 ()
  (let* ((client (wsd:make-client (uiop:strcat "wss://testnet.bitmex.com" "/realtime"))))
    (progn (wsd:start-connection client)           
           (wsd:send client (cl-json:encode-json-plist-to-string (list "op" "subscribe" "args" (list "orderBookL2_25:XBTUSD"))))
           (wsd:on :message client
                   (lambda (message)
                     (format t "~&Got: ~A~%" message)))
           (sleep 3)
           (wsd:close-connection client))))

;; Function: "/realtime"
;; Symbol: "XBTUSD"
;; Method: "GET"
;; listOfArguments: (list "symbol" "depth")
(defun test3 (function method listOfArguments)
  (let* ((client (wsd:make-client (uiop:strcat "wss://testnet.bitmex.com" function)))
         (expires (bitMEXsignature:generate-nonce))
         (signature (bitMEXsignature:api-signature
                     "u-bfQxmU9sLwjGSMRKQkMrLuaEfMeFK-0D0Eb9lJJQxM_5jB"
                     method
                     function
                     expires
                     "")))
    (progn (wsd:start-connection client)
           (wsd:send client (cl-json:encode-json-plist-to-string (list "op" "authKeyExpires" "args" (list "ilgNx1_H8GmBOB26jFlX9Aea" expires signature))))
           (wsd:on :message client
                   (lambda (message)
                     (format t "~&Got: ~A~%" message)))
           (wsd:send client (cl-json:encode-json-plist-to-string (list "op" "subscribe" "args" listOfArguments)))
           (wsd:on :message client
                   (lambda (message)
                     (format t "~&Got: ~A~%" message)))
           (wsd:close-connection client))))
