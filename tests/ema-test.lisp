(defpackage ema-test (:use :cl :prove))
(in-package :ema-test)

(prove:plan 1)

;; Using values from https://school.stockcharts.com/doku.php?id=technical_indicators:moving_averages spreadsheet
(prove:is (ema (list 22.15 22.39 22.38 22.61 23.36 24.05 23.75 23.83 23.95 23.63 23.82 23.87 23.65 23.19 23.10 23.33 22.68 23.10 22.40 22.17) 2/11 22.22)
          #(22.207272 22.240496 22.265862 22.328434 22.515991 22.794903 22.968557 23.125183 23.27515 23.339668 23.427002 23.507547 23.533447 23.471003
            23.403547 23.390175 23.261051 23.23177 23.080538 22.914986))

(finalize)
