#|
  This file is a part of bitmextrader project.
|#

(defsystem bitmextrader
  :version "0.1.0"
  :author "Michael McClellan"
  :serial t
  :license ""
  :depends-on ("ironclad" "quri" "cl-json" "websocket-driver-client" "trivia" "json-streams" "jsown" "drakma" "yason" "cl-date-time-parser" "cl-async" "blackbird")
  :components ((:module "src"
                        :components
                        ((:file "settings")
                         (:file "bitMEXsignature")                         
                         (:file "ws")
                         (:file "rest")
                         (:file "rsi")
                         (:file "ema")
                         (:file "queue")
                         (:file "bitmextraderMain"))))
  :description ""
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.md"))
  :in-order-to ((test-op (test-op "bitmextrader-tests"))))

